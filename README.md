# University of Guelph Robotics Team

We are a group of highly motivated undergraduate students, interested in gaining hands on experience and applying theories learned in the classroom on robotic systems. As such, we established our undergraduate robotics team. The robotics team is mainly targeted towards Engineering Science & Computing and Mechanical Engineering; however, is not limited to only these disciplines.

Our team's focus is autonomous mobile robots. The final goal for our team is to establish a strong robotics team on campus to design and build autonomous mobile robots with advanced features for participation in prestigious competitions such as the Intelligent Ground Vehicle Competition (IGVC).

If you are interested in participating in robotics team please contact ugrt@uoguelph.ca for meeting times and how to get involved.

